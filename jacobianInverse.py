import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.figure
import matplotlib.patches
import scipy.linalg
from scipy import signal
from RobotArm import *
from scipy.integrate import odeint

# Instantiate robot arm class.
Arm = RobotArm2D()
xdata = []
x2data = []
ydata = []
# Add desired number of joints/links to robot arm object.
# unit is centimeter (cm)
Arm.add_revolute_link(length=37.8, thetaInit=math.radians(10))
Arm.add_revolute_link(length=46.0, thetaInit=math.radians(0))
Arm.add_revolute_link(length=0, thetaInit=math.radians(0))
Arm.update_joint_coords()

# parameter for report 2
kp1 = 5
kp2 = 5
kp3 = 5
inertia1 = 1.0 * (6 * 6 + 37 * 37 + 46 * 46)
inertia2 = 1.0 * (6 * 6 + 46 * 46)
inertia3 = 1.0 * (6 * 6)
viscosity = 100.0
theta = 0.0
du = 1

# Initialize target coordinates to current end effector position.
target = Arm.joints[:, [-1]]
initial_condition = 0
reset = 0

# Initialize plot and line objects for target, end effector, and arm.
fig, ax = plt.subplots(figsize=(5, 5))
fig.subplots_adjust(left=0, bottom=0, right=1, top=1)
targetPt, = ax.plot([], [], marker='o', c='r')
endEff, = ax.plot([], [], marker='o', markerfacecolor='w', c='g', lw=2)
armLine, = ax.plot([], [], marker='o', c='g', lw=2)

fig2, ax2 = plt.subplots(figsize=(5, 5))
x_axis, = ax2.plot([], [])
fig3, ax3 = plt.subplots(figsize=(5, 5))
x2_axis, = ax3.plot([], [])

# Determine maximum reach of arm.
reach = sum(Arm.lengths)

# Set axis limits based on reach from root joint.
ax.set_xlim(Arm.xRoot - 1.2 * reach, Arm.xRoot + 1.2 * reach)
ax.set_ylim(Arm.yRoot - 1.2 * reach, Arm.yRoot + 1.2 * reach)
ax2.set_xlim(-(Arm.xRoot + 1.2 * reach), Arm.xRoot + 1.2 * reach)
ax2.set_ylim(0, 2000)
ax3.set_xlim(-(Arm.xRoot + 1.2 * reach), Arm.xRoot + 1.2 * reach)
ax3.set_ylim(0, 2000)

# Add dashed circle to plot indicating reach.
circle = plt.Circle((Arm.xRoot, Arm.yRoot), reach, ls='dashed', fill=False)
ax.add_artist(circle)


def update_plot():
    """Update arm and end effector line objects with current x and y
        coordinates from arm object.
    """
    armLine.set_data(Arm.joints[0, :-1], Arm.joints[1, :-1])
    endEff.set_data(Arm.joints[0, -2::], Arm.joints[1, -2::])
    J = Arm.get_jacobian()
    U, S, V = np.linalg.svd(J)

    xdata.append(Arm.joints[:3, [-1]][0])   # append new x-axis data to plot x-axis manipulability measure
    x2data.append(Arm.joints[:3, [-1]][1])  # append new y-axis data to plot y-axis manipulability measure
    ydata.append(S[0]*S[1])
    manipulability = matplotlib.patches.Ellipse((Arm.joints[:3, [-1]][0], Arm.joints[:3, [-1]][1]), np.sqrt(S[0]),
                                                np.sqrt(S[1]), np.arctan(U[0][1] / U[0][0]) * 180 / np.pi, fill=False)
    ax.add_artist(manipulability)
    x_axis.set_data(xdata, ydata)
    x2_axis.set_data(x2data, ydata)


update_plot()
idx = 0


def move_to_target():
    """Run Jacobian inverse routine to move end effector toward target."""
    global Arm, target, reach, idx

    # Set distance to move end effector toward target per algorithm iteration.
    #    distPerUpdate = 0.01 * reach

    num1 = [1]
    num2 = [1]
    num3 = [1]
    den1 = [inertia1, viscosity, kp1]
    den2 = [inertia2, viscosity, kp2]
    den3 = [inertia3, viscosity, kp3]
    sys1 = signal.TransferFunction(num1, den1)
    sys2 = signal.TransferFunction(num2, den2)
    sys3 = signal.TransferFunction(num3, den3)
    t1, y1 = signal.step(sys1)
    t2, y2 = signal.step(sys2)
    t3, y3 = signal.step(sys3)

    if idx < 100:
        deltaTheta = np.array([y1[idx], y2[idx], y3[idx]])
        idx += 1

        Arm.thetas = deltaTheta.flatten()
        Arm.update_joint_coords()
        update_plot()
    else:
        print((Arm.thetas[0]+Arm.thetas[1]) * 84)


# "mode" can be toggled with the Shift key between 1 (click to set
# target location) and -1 (target moves in predefined motion).
mode = -1


def on_button_press(event):
    """Mouse button press event to set target at the location in the
        plot where the left mousebutton is clicked.
    """
    global target, targetPt
    xClick = event.xdata
    yClick = event.ydata

    # Ensure that the x and y click coordinates are within the axis limits
    # by checking that they are floats.
    if (mode == 1 and event.button == 1 and isinstance(xClick, float)
            and isinstance(yClick, float)):
        targetPt.set_data(xClick + 0.1, yClick)
        target = np.array([[xClick + 0.1, yClick, 0, 1]]).T

    elif mode == -1 and initial_condition % 2 == 1:
        targetPt.set_data(reach, 0)
        target = np.array([[reach, 0, 0, 1]]).T


fig.canvas.mpl_connect('button_press_event', on_button_press)

# Use "exitFlag" to halt while loop execution and terminate script.
exitFlag = False


def on_key_press(event):
    """Key press event to stop script execution if Enter is pressed,
        or toggle mode if Shift is pressed.
    """
    global exitFlag, mode
    if event.key == 'enter':
        exitFlag = True
    elif event.key == 'shift':
        mode *= -1


fig.canvas.mpl_connect('key_press_event', on_key_press)

# Turn on interactive plotting and show plot.
plt.ion()
plt.show()

print('Select plot window and press Shift to toggle mode or press Enter to quit.')

# Variable "t" is used for moving target mode.
while not exitFlag:
    if mode == -1 and initial_condition % 2 == 0:
        # initial position
        initial_condition += 1
#        targetPt.set_data(reach, 0)
#        target = np.array([[reach, 0, 0, 1]]).T

    move_to_target()
    fig.canvas.get_tk_widget().update()
